const laptopsElement = document.getElementById("laptops");
const specsElement = document.getElementById("specs");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const imageElement = document.getElementById("image");
const stockElement = document.getElementById("stock");
const buyElement = document.getElementById("buy");

let laptops = [];
selectedLaptopPrice = 0;

//Function to fetch Komputer API
fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

//Add laptops to dropdown menu
const addLaptopsToMenu=(laptops) => {
    laptops.forEach(laptop => addLaptopToMenu(laptop));
  
}

const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.text = laptop.title;
    laptopsElement.appendChild(laptopElement);
    getInformation(laptops[0])
    
 
}

//Function to change the information to the selected laptop
function handleLaptopInformationChange(e) {
    const selectedLaptop = laptops[e.target.selectedIndex];
    getInformation(selectedLaptop)
}

function getInformation (laptop) {
    specsElement.innerText = laptop.specs;
    titleElement.innerText = laptop.title;
    descriptionElement.innerText =laptop.description;
    priceElement.innerText = 'Price: ' + 'NOK ' + laptop.price;
    stockElement.innerText = 'Stock: ' + laptop.stock;
    imageElement.setAttribute("src", "https://hickory-quilled-actress.glitch.me/" + laptop.image )
    selectedLaptopPrice = laptop.price;
}



//Function to buy a laptop
function buyLaptop() {
    console.log(selectedLaptopPrice)
    if (selectedLaptopPrice > loan.currentTotalBalance) {
        alert('You can not afford this Komputer')
   }
    else {
        loan.currentTotalBalance = parseInt(loan.currentTotalBalance) - parseInt(selectedLaptopPrice);
        alert('You just bought a Komputer! Congratulations! ')
        setInformationBank()
        setInformationLoan()
    }


}

laptopsElement.addEventListener("change", handleLaptopInformationChange);
buyElement.addEventListener("click", buyLaptop);