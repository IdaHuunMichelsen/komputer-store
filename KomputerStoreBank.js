const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan");
const getLoanElement = document.getElementById("getLoan");
const totalBalanceElement = document.getElementById("totalBalance");
const loan = {currentTotalBalance:0, totalLoanAmount:0, startLoan:0};

class KomputerStoreBAnk{
    constructor(balance, loan, totalLoan){
        this.balance = balance;
        this.loan = loan;
        this.totalLoan=totalLoan;
    }
}

//Function that sets and shows the initial bank balance
function setInformationBank(){
    balanceElement.innerText = 'Current bank balance: ' + 'NOK ' + parseInt(loan.currentTotalBalance);
}

//Function that sets the and shows the initial loan balance
function setInformationLoan(){
    loanElement.innerText = 'Current loan: ' + ' NOK ' + loan.totalLoanAmount;
}

//function to get a loan
function getALoan(){
    if(loan.currentTotalBalance ==0){
        alert("You need to transfer money to get a loan");
        return;
    }
    else if(loan.totalLoanAmount > 0){
        alert("You already have an outstanding loan")
    }
    else{
        while(true){
            let input = prompt("Set loan amount: ");
            if(input > 2 * loan.currentTotalBalance){
                alert("You can only loan double the amount of your current balance");
                return true;
            }
            else if (input == 0) {
                alert ("You need to enter an amount");
            }
            else if (input.length <= 0 || isNaN(input)){
                alert("Please enter the loan amount to continue");
            }
            else
                alert("Loan has been submitted")
                loan.startLoan = input;
                loan.totalLoanAmount = input;
                loan.currentTotalBalance =(parseInt(loan.startLoan) + parseInt(loan.currentTotalBalance));
                repayElement.hidden = false

                setInformationBank()
                setInformationLoan()

                return true;
        }
    }
}
setInformationBank()
setInformationLoan()

getLoanElement.addEventListener("click", getALoan)