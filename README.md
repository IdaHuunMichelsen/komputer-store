**About the project**

Dynamic webpage made with "vanilla" JavaScript, HTML and CSS. I have also used Bootstrap. 

**Functionalities**

*The Bank*
- Shows the bank balance.
- Shows the outstanding loan value.
- It is possible to ask for a loan.
    - You can only get a loan more than double of your bank balance.
    - It is not possible to have more than one loan at a time. 

*Work*
- The work button increase your Pay Balance with 100 NOK on each click.
- It is possible to transfer the money to the Bank by clicking the "Transfer to bank"-button. 
- Once you have a loan a "Repay loan"-button apperas. When clicking on this button the full value of your current Pay amount goes towards repaying your loan. The remaining funds after repaying your loan can be transferred to your Bank account. 

*Laptop Selection*
- Lets you choose between different laptops. 
- Has an info section showing the different features of the chosen laptop, name, price and stock. 
- The "Buy Now"-button allows you to buy a laptop if you have enought money in your bank account. If you don't have enough money, you are not able to buy it. 

**Contributors**

Ida Huun Michelsen



