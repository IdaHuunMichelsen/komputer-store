const showBalanceElement = document.getElementById("salary");
const workElement = document.getElementById("work");
const transferElement = document.getElementById("transfer");
const repayElement = document.getElementById("repay");

let currentSalary = 0;

//Function to show the current balance
function showBalance() {
    currentSalary.innerText = 'Current salary: ' + 'NOK ' + currentSalary;
}

//Function to update salary with 100 NOK
function handleSalary() {
    currentSalary = currentSalary + 100;
    updateSalary();
}

//Function to update the salary in the show salary function
function updateSalary() {
    salary.innerText = 'Current salary: ' + 'NOK ' + currentSalary;
}

//Function to transfer money to the bank
function handleTransfer() {
    if (currentSalary > 0 && loan.totalLoanAmount != 0) {
        newLoanPayment = currentSalary * 0.10;
        newCurrentSalary = currentSalary - newLoanPayment

        loan.totalLoanAmount = loan.totalLoanAmount - newLoanPayment;
        loan.currentTotalBalance = loan.currentTotalBalance + newCurrentSalary;
        currentSalary = 0
        updateSalary();
        setInformationBank()
        setInformationLoan()
    }
    else if (currentSalary > 0 && loan.totalLoanAmount == 0) {
        loan.currentTotalBalance = loan.currentTotalBalance + currentSalary;
        currentSalary = 0;
        updateSalary();
        setInformationBank()
        setInformationLoan()
    }
    else {
        alert("You don't have money to transfer to the bank")
    }
}

//Function to repay loan
function repayLoan() {
    if (loan.totalLoanAmount < currentSalary) {
        currentSalary = currentSalary - loan.totalLoanAmount
        loan.totalLoanAmount = 0;
        repayElement.hidden = true
        updateSalary();
        setInformationBank()
        setInformationLoan()
    }
    else {
        loan.totalLoanAmount = loan.totalLoanAmount - currentSalary;
        currentSalary = 0;
        updateSalary();
        setInformationBank()
        setInformationLoan()
    }
}

workElement.addEventListener("click", handleSalary);
transferElement.addEventListener("click", handleTransfer);
showBalance()
repayElement.addEventListener("click", repayLoan);


